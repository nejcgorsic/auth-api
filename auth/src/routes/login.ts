import express, { Request, Response } from 'express';
import { PasswordManager } from '../services/password';
import { User } from '../models/user';
import { validateIncomingRequest } from '../middlewares/validate-request';
import { body } from 'express-validator';
import jwt from 'jsonwebtoken';
import { BadRequestError } from '../errors/bad-request-error';

const router = express.Router();
router.post(
  '/api/login',
  [
    body('email')
      .isEmail()
      .withMessage('A valid email must be provided'),
    body('password')
      .trim()
      .notEmpty()
      .withMessage('Enter a password')
  ],
  validateIncomingRequest,
  async (req: Request, res: Response) => {

    const { body: { email, password } } = req;

    const existingUser = await User.findOne({ email });
    if (!existingUser) {
      throw new BadRequestError('Login failed');
    }

    const doPasswordsMatch = await PasswordManager.comparePasswords(existingUser.password, password);
    if (!doPasswordsMatch) {
      throw new BadRequestError('Invalid credentials');
    }

    // Generate JWT
    const userJwt = jwt.sign(
      {
        id: existingUser.id,
        email: existingUser.email
      },
      process.env.JWT_KEY!
    );
    // Store it on session object.
    req.session = {
      jwt: userJwt
    };

    // User is logged in. Send them a cookie.
    res.status(200).send(existingUser);
  }
);

export { router as loginRouter };
