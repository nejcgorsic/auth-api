import express from 'express';
import { User } from '../models/user';
const router = express.Router();
import { BadRequestError } from '../errors/bad-request-error';

router.put(
  '/api/user/:id/unlike',
  async (req, res) => {
    if (!req.session?.jwt) {
      return res.send({ message: 'Login first to unlike an user!' });
    }
    const { params: { id } } = req;

    try {
      await User.findByIdAndUpdate(
        { _id: id },
        { $inc: { likes: -1 } },
        {
          new: true,
          runValidators: true
        }
      );
      res.send({
        message: 'Successfully unliked an user!'
      });
    } catch (err) {
      throw new BadRequestError('User with that ID not found');
    }
  }
);

export { router as unlikeAnUserRouter };
