// ! TODO: Update this logic with JWT auth

import express, { Request, Response } from 'express';
import { PasswordManager } from '../services/password';
import { User } from '../models/user';
import { BadRequestError } from '../errors/bad-request-error';
const router = express.Router();

router.post(
  '/api/me/update-password',
  async (req: Request, res: Response) => {
    const { body: { email, password: bodyPassword, newPassword } } = req;
    const existingUser = await User.findOne({ email });

    if (!existingUser) {
      throw new BadRequestError('Users doesnt exist');
    }

    const doPasswordsMatch = await PasswordManager.comparePasswords(existingUser.password, bodyPassword);
    if (!doPasswordsMatch) {
      throw new BadRequestError('Previous password does not match');
    }

    const newUserPassword = await PasswordManager.toHash(newPassword);
    try {
      await User.findOneAndUpdate(
        { email: email },
        { password: newUserPassword },
      );
      res.send({
        message: 'Successfully updated!'
      });
    } catch (err) {
      return console.log(err);
    }
  }
);

export { router as updatePasswordRouter };
