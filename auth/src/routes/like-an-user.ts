import express from 'express';
import { User } from '../models/user';
const router = express.Router();
import { BadRequestError } from '../errors/bad-request-error';

router.put(
  '/api/user/:id/like',
  async (req, res) => {
    if (!req.session?.jwt) {
      return res.send({ message: 'Login first to like an user!' });
    }
    const { params: { id } } = req;

    try {
      await User.findByIdAndUpdate(
        { _id: id },
        { $inc: { likes: 1 } },
        {
          new: true,
          runValidators: true
        }
      );
      res.send({
        message: 'Successfully liked an user!'
      });

    } catch (err) {
      throw new BadRequestError('User with that ID not found');
    }
  }
);

export { router as likeAnUserRouter };
