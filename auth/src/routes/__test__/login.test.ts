import request from 'supertest';
import { app } from '../../app';

it('Fails when an email when an email does not exist is supplied', async () => {
  return request(app)
    .post('/api/login')
    .send({
      email: 'asdasdasda@test.com',
      password: 'password'
    })
    .expect(400);
});

it('Incorrect password supplied => fail', async () => {
  await request(app)
    .post('/api/signup')
    .send({
      email: 'test@gmail.com',
      password: 'password'
    })
    .expect(201);

  await request(app)
    .post('/api/login')
    .send({
      email: 'test@gmail.com',
      password: 'password123'
    })
    .expect(400);
});

it('Respond with cookie when correct credentials are supplied', async () => {
  await request(app)
    .post('/api/signup')
    .send({
      email: 'test123@test.com',
      password: 'password'
    })
    .expect(201);

  const response = await request(app)
    .post('/api/login')
    .send({
      email: 'test123@test.com',
      password: 'password'
    })
    .expect(200);

  expect(response.get('Set-Cookie')).toBeDefined();
});
