import request from 'supertest';
import { app } from '../../app';

it('Successfull login => return 400', async () => {
  return request(app)
    .post('/api/signup')
    .send({
      email: 'test@gmail.com',
      password: 'password'
    })
    .expect(201);
});

it('Invalid email => return 400', async () => {
  return request(app)
    .post('/api/signup')
    .send({
      email: 'testgmailll.com',
      password: 'password'
    })
    .expect(400);
});

it('Invalid password => return 400', async () => {
  return request(app)
    .post('/api/signup')
    .send({
      email: 'test@gmail.com',
      password: 'pa'
    })
    .expect(400);
});

it('Missing email and pass => return 400', async () => {
  await request(app)
    .post('/api/signup')
    .send({ email: 'test@gmail.com' })
    .expect(400);

  await request(app)
    .post('/api/signup')
    .send({ password: 'password' })
    .expect(400);

  await request(app)
    .post('/api/signup')
    .send({})
    .expect(400);
});

it('Do not allow duplicate emails', async () => {
  await request(app)
    .post('/api/signup')
    .send({
      email: 'test@gmail.com',
      password: 'password'
    })
    .expect(201);

  await request(app)
    .post('/api/signup')
    .send({
      email: 'test@gmail.com',
      password: 'password'
    })
    .expect(400);
});

it('Set cookie after signup', async () => {
  const response = await request(app)
    .post('/api/signup')
    .send({
      email: 'test@test.com',
      password: 'password'
    })
    .expect(201);

  expect(response.get('Set-Cookie')).toBeDefined();
});
