import express from 'express';
import { User } from '../models/user';
const router = express.Router();

router.get(
  '/api/most-liked',
  async (req, res) => {
    const query = await User.find({}).sort({ likes: -1 });
    const results = await query;
    res.send({
      results: results
    });
  }
);

export { router as mostLikedRouter };
