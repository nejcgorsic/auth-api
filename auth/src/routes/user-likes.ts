import express, { Request, Response } from 'express';
import { User } from '../models/user';
import { BadRequestError } from '../errors/bad-request-error';
const router = express.Router();

router.get(
  '/api/user/:id',
  async (req: Request, res: Response) => {
    const { params: { id } } = req;
    const user = await User.findById(id);

    if (!user) {
      throw new BadRequestError('No user with that id');
    }
    res.send({
      email: user?.email,
      likes: user?.likes
    });
  }
);

export { router as numberOfUserLikes };
