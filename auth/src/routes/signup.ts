import express, { Request, Response } from 'express';
import { body } from 'express-validator';
import jwt from 'jsonwebtoken';

import { validateIncomingRequest } from '../middlewares/validate-request';
import { User } from '../models/user';
import { BadRequestError } from '../errors/bad-request-error';

const router = express.Router();

router.post(
  '/api/signup',
  [
    body('email')
      .isEmail()
      .withMessage('A valid email must be provided'),
    body('password')
      .trim()
      .isLength({ min: 4, max: 20 })
      .withMessage('Password must be between 4 and 20 characters')
  ],
  validateIncomingRequest,
  async (req: Request, res: Response) => {

    const { body: { email, password } } = req;

    // Respond with error if user exists
    const existingUser = await User.findOne({ email });
    if (existingUser) {
      throw new BadRequestError('Email already in use!');
    }

    // Hash the password

    // Create new user and save to MongoDB
    const user = User.build({ email, password });
    await user.save();

    // Generate JWT
    const userJwt = jwt.sign(
      {
        id: user.id,
        email: user.email
      },
      process.env.JWT_KEY!
    );

    // Store it on session object.
    req.session = {
      jwt: userJwt
    };

    // User is logged in. Send them a cookie.
    res.status(201).send(user);
  }
);

export { router as signUpRouter };
