/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import mongoose from 'mongoose';
import { PasswordManager } from '../services/password';

// Properties requiured to create a new user.
interface UserAttributes {
  email: string;
  password: string;
}

// User model interface attributes
// Whole collection of users
interface UserModel extends mongoose.Model<UserDocument> {
  build(attrs: UserAttributes): UserDocument;
}

// An interface that describes the properties that a user document has
// single user.
interface UserDocument extends mongoose.Document {
  email: string;
  password: string;
  likes: number;
}

const userSchema = new mongoose.Schema({
  email: {
    type: String, // specific to mongoose not TypeScript.
    required: true
  },
  password: {
    type: String,
    required: true
  },
  likes: {
    type: Number,
    default: 0,
    min: [0, 'Minimum number of likes is 0']
  }
}, {
  toJSON: {
    transform(doc, ret) {
      ret.id = ret._id;
      delete ret._id;
      delete ret.password;
      delete ret.__v;
    }
  }
});

userSchema.pre('save', async function (done) {
  if (this.isModified('password')) {
    const hashed = await PasswordManager.toHash(this.get('password')); // users password from the document
    this.set('password', hashed);
  }

  done();
});

// Custom function built into the model.
userSchema.statics.build = (attrs: UserAttributes) => {
  return new User(attrs);
};

const User = mongoose.model<UserDocument, UserModel>('User', userSchema);

export { User };
