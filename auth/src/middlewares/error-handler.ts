import { Response, NextFunction, Request } from 'express';
import { CustomError } from '../errors/custom-error';

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const errorHandler = (
  err: Error,
  req: Request,
  res: Response,
  next: NextFunction
) => {

  if (err instanceof CustomError) {
    return res.status(err.statusCode).send({ errors: err.serializeErrors() });
  }

  return res.status(400).send({
    errors: [{
      message: 'Something went wrong'
    }]
  });
};
