import { MongoMemoryServer } from 'mongodb-memory-server';
import mongoose from 'mongoose';
import { app } from '../app';
let mongo: any;

// Connect to mongodb instance in memory
beforeAll(async () => {
  process.env.JWT_KEY = 'auth-api';

  mongo = new MongoMemoryServer();
  const mongoUri = await mongo.getUri();

  await mongoose.connect(mongoUri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  });
});

// Delete all connections for each
beforeEach(async () => {
  const collections = await mongoose.connection.db.collections();

  for (const collection of collections) {
    await collection.deleteMany({});
  }
});

// After all tests are complete
afterAll(async () => {
  await mongo.stop();
  await mongoose.connection.close();
});
