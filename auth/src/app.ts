// ! Application configuration

import express from 'express';
import 'express-async-errors';
import { json } from 'body-parser';
import cookieSession from 'cookie-session';

import { currentUserRouter } from './routes/current-user';
import { numberOfUserLikes } from './routes/user-likes';
import { loginRouter } from './routes/login';
import { signUpRouter } from './routes/signup';
import { updatePasswordRouter } from './routes/update-password';
import { likeAnUserRouter } from './routes/like-an-user';
import { unlikeAnUserRouter } from './routes/unlike-an-user';
import { mostLikedRouter } from './routes/most-liked';
import { errorHandler } from './middlewares/error-handler';
import { NotFoundError } from './errors/not-found-error';

const app = express();
app.set('trust proxy', true);
app.use(json());
app.use(
  cookieSession({
    signed: false,
    secure: process.env.NODE_ENV !== 'test'
  })
);

app.use(currentUserRouter);
app.use(loginRouter);
app.use(signUpRouter);
app.use(updatePasswordRouter);
app.use(numberOfUserLikes);
app.use(likeAnUserRouter);
app.use(unlikeAnUserRouter);
app.use(mostLikedRouter);

app.all('*', async (req, res) => {
  throw new NotFoundError();
});

app.use(errorHandler);

export { app };
