// ! Start application

import mongoose from 'mongoose';
import { app } from './app';

const _PORT = 3000;

const start = async () => {
  if (!process.env.JWT_KEY) {
    throw new Error('JWT_KEY must be defined in env variables!');
  }

  try {
    await mongoose.connect('mongodb://auth-mongo-srv:27017/auth', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true
    });
    console.log('Connected to MongoDB.');
  } catch (err) {
    console.error(err);
  }

  app.listen(_PORT, () => {
    console.log(`Server running on port ${_PORT}!`);
  });

};

start();
