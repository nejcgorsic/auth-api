/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { CustomError } from './custom-error';

export class DatabaseConnectionError extends CustomError {
  statusCode = 500;
  reason = 'Error while connecting to the database';

  constructor() {
    super('Error while connecting to the database');

    // Typescript issue workaround, because we are extending a built in class.
    Object.setPrototypeOf(this, DatabaseConnectionError.prototype);
  }

  serializeErrors() {
    return [{
      message: this.reason
    }];
  }
}
