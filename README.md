## API Server description.
API is built using node.js and typescript. It uses MongoDB as a database as mongo represents data as a collection of documents rather than tables related by foreign keys. Also greater support for this particular stack across the web (NodeJS / MongoDB).

The API is implemented with kubernetes and runs "out-of-the-box" and is built with the microservice architecture in mind.
How to run application:
* Download skaffold dev https://skaffold.dev/
* Generate kubernetes secret: ``kubectl create secret generic jwt-secret-ap --from-literal=JWT_KEY=auth-api`` (getting secrets: ``kubectl get secrets``)
* Add ``127.0.0.1 auth-api.dev`` to hosts file 
* run command ``skaffold dev`` in root of repo.

**You will need to have kubernetes enabled on your local machine in docker client**


## Notes:
* If you delete or restart the pod running MongoDB, we lose all of the data in it.

## Postman:
* In root of repo postman collection is added which you can import to postman to test the application.
